# PhysicsDemo
A small physics sandbox program written in Java, using the Processing and JBox2D libraries.

## Controls
s: Switch between single, and continuous spawn mode.

g: Toggle gravity.

c: Clear all objects.

1: Switch between object presets.

ESC: Close the program.

