package com.venumwolf.physicsdemo;

import com.venumwolf.physicsdemo.physicsobjects.AbstractPhysicsObject;
import com.venumwolf.physicsdemo.physicsobjects.dynamic.simple.Ball;
import com.venumwolf.physicsdemo.physicsobjects.dynamic.simple.Box;
import com.venumwolf.physicsdemo.physicsobjects.dynamic.simple.MouseJointSpring;
import com.venumwolf.physicsdemo.physicsobjects.fixed.simple.Wall;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PVector;
import processing.event.MouseEvent;
import processing.opengl.PJOGL;
import shiffman.box2d.Box2DProcessing;

import java.util.ArrayList;

public class PhysicsDemo extends PApplet {
    // Static PhysicsDemo reference: Allows other classes to access the program.
    static PhysicsDemo physicsDemo = null;

    // Global Box2D reference.
    protected Box2DProcessing box2D;

    // World object ArrayList.
    protected ArrayList<AbstractPhysicsObject> worldObjects;
    protected ArrayList<AbstractPhysicsObject> worldObjectsToClear;

    // Global physics settings.
    protected Vec2 physicsGravity;
    protected float physicsScaleFactor;

    // Global render / UI settings.
    protected int antiAliasAmount;
    protected PFont uiFont;

    // Global control references.
    protected boolean objectClicked;
    protected int spawnPresetIndex;
    protected boolean singleSpawn;
    protected PVector clickStart;
    protected PVector clickStop;
    AbstractPhysicsObject clickedObject;

    public PhysicsDemo() {
        super();
        physicsDemo = this;
    }

    /**
     * Get the current active instance of the PhysicsDemo.
     * This allows other parts of the application to access the main instance.
     * <br>
     * @return The active PhysicsDemo object.
     */
    public static PhysicsDemo getPhysicsDemo() {
        return physicsDemo;
    }

    @Override
    public void settings() {
        fullScreen(P3D);
        smooth(2);
    }

    @Override
    public void setup() {
        // Initialize render and UI settings.
        antiAliasAmount = 8;

        // Initialize controls.
        objectClicked = false;
        clickStart = null;
        clickStop = null;
        clickedObject = null;

        // Initialize physics simulation settings (earth gravity by default.)
        physicsGravity = new Vec2(0.0f, -9.807f);
        physicsScaleFactor = 45.0f;

        // Initialize physics world.
        box2D = new Box2DProcessing(this, physicsScaleFactor);
        box2D.createWorld();
        box2D.setGravity(physicsGravity.x, physicsGravity.y);

        // Initialize global object array.
        worldObjects = new ArrayList<AbstractPhysicsObject>();
        worldObjectsToClear = new ArrayList<AbstractPhysicsObject>();

        // Spawning.
        spawnPresetIndex = 0;
        singleSpawn = true;

        // Initialize renderer and UI.
        uiFont = createFont("Consolas", 12);

        // Force VSync
        frameRate(1000);
        PJOGL pgl = (PJOGL) beginPGL();
        pgl.gl.setSwapInterval(1);
        endPGL();

        // Create outside walls.
        worldObjects.add(new Wall(width / 2, height, width, 0.5f, false)); // Immutable floor.
        worldObjects.add(new Wall(width / 2, -0, width, 0.5f, false)); // Immutable ceiling.
        worldObjects.add(new Wall(0, height / 2, 0.5f, height, false)); // Immutable wall.
        worldObjects.add(new Wall(width, height / 2, 0.5f, height, false)); // Immutable wall.
    }

    /**
     * Steps physics simulation, draws objects on screen.
     */

    @Override
    public void draw() {
        box2D.step(1.0f / frameRate, 20, 20);

        if (mousePressed && mouseButton == LEFT && !singleSpawn) {
            clickStop = new PVector(mouseX, mouseY);
            spawnObjectFromPreset();
            clickStop = null;
        }

        background(0);

        for (AbstractPhysicsObject object : worldObjects) {
            object.update();
            object.display();
        }

        fill(255);
        textFont(uiFont, 12);

        text(PApplet.parseInt(frameRate) + " fps", 30, 40);

        text("Spawn preset: " + spawnPresetIndex, 30, 54);

        text("Single spawn: " + singleSpawn, 30, 68);

        text("Gravity: " + box2D.world.getGravity().y * -1 + " ms/s^2", 30, 82);

        if (clickStart != null) {
            stroke(255);
            strokeWeight(1);
            line(clickStart.x, clickStart.y, mouseX, mouseY);
        }

        if (worldObjectsToClear.size() > 0)
            clearQueuedWorldObjects();
    }

    @Override
    public void mousePressed() {
        if (mouseButton == LEFT) {
            clickStart = new PVector(mouseX, mouseY);
        } else if (mouseButton == RIGHT) {
            clickedObject = null;
            for (AbstractPhysicsObject object : worldObjects) {
                if (object.contains(mouseX, mouseY)) {
                    clickedObject = object;

                    MouseJointSpring mouseJoint = new MouseJointSpring();
                    mouseJoint.bind(mouseX, mouseY, clickedObject);
                    worldObjects.add(mouseJoint);

                    objectClicked = true;

                    break;
                }
            }
        }
    }

    @Override
    public void mouseReleased() {
        if (mouseButton == LEFT && singleSpawn) {
            clickStop = new PVector(mouseX, mouseY);
            spawnObjectFromPreset();
        }
        clickStart = null;
        clickStop = null;
        for (AbstractPhysicsObject object : worldObjects) {
            if (object instanceof MouseJointSpring) {
                worldObjectsToClear.add(object);
            }
        }
    }

    @Override
    public void mouseWheel(MouseEvent event) {
        if (clickedObject != null) {
            Body objectBody = clickedObject.getBody();
            float newAngle = objectBody.getAngle() + ((event.getCount() * (PI / 180)) * 5);
            objectBody.setTransform(objectBody.getWorldCenter(), newAngle);
        }
    }

    @Override
    public void keyPressed() {
        char pressedKey = Character.toLowerCase(key);
        if (pressedKey == 'c' && !mousePressed) {
            worldObjectsToClear = (ArrayList<AbstractPhysicsObject>) worldObjects.clone();
        }
        if (pressedKey == 's')
            singleSpawn = !singleSpawn;
        if (pressedKey == '1')
            cycleSpawnedObject();
        if (pressedKey == 'g')
            toggleGravity();
    }

    public void toggleGravity() {
        if (box2D.world.getGravity().x == 0 && box2D.world.getGravity().y == 0) {
            box2D.world.setGravity(physicsGravity);
        } else {
            box2D.world.setGravity(new Vec2(0, 0));
        }
    }

    /**
     * Spawn object from currently selected preset.
     * TODO: Replace this system with something more flexible, perhaps add support for JSON or YAML configs.
     */
    public void spawnObjectFromPreset() {
        switch (spawnPresetIndex) {
            case 0:
                worldObjects.add(
                        new Ball(
                                clickStart.x,
                                clickStart.y,
                                0.2426f,
                                color(0, 255, 0, 100),
                                color(0, 255, 0),
                                1,
                                86.3f,
                                0.8f,
                                0.8f,
                                box2D.scalarPixelsToWorld((clickStart.x - clickStop.x) * -1) + random(-1, 1),
                                box2D.scalarPixelsToWorld((clickStart.y - clickStop.y)) + random(-1, 1),
                                0.0f
                        )
                );
                // worldObjects.add(new CannonBall(mouseX, mouseY));
                break;
            case 1:
                worldObjects.add(new Box(clickStart.x,
                                clickStart.y,
                                1.0f,
                                1.0f,
                                130.0f,
                                0.8f,
                                0.2f,
                                box2D.scalarPixelsToWorld((clickStart.x - clickStop.x) * -1),
                                box2D.scalarPixelsToWorld((clickStart.y - clickStop.y)),
                                0.0f
                        )
                );
                break;
            case 2:
                worldObjects.add(
                        new Box(
                                clickStart.x,
                                clickStart.y,
                                0.5f,
                                4.0f,
                                130.0f,
                                0.8f,
                                0.2f,
                                box2D.scalarPixelsToWorld((clickStart.x - clickStop.x) * -1),
                                box2D.scalarPixelsToWorld((clickStart.y - clickStop.y)),
                                0.0f
                        )
                );
                break;
            default:
                break;
        }
    }

    /**
     * Cycles though the spawn presets. (It may be best to refine this solution later.)
     */
    public void cycleSpawnedObject() {
        switch (spawnPresetIndex) {
            case 0:
                spawnPresetIndex++;
                break;
            case 1:
                spawnPresetIndex++;
                break;
            default:
                spawnPresetIndex = 0;
                break;
        }
    }

    /**
     * Clears all queued objects from the world, then clears worldObjectsToClear ArrayList.
     * Objects to clear should be set in global worldObjectsToClear ArrayList.
     */
    public void clearQueuedWorldObjects() {
        for (AbstractPhysicsObject object : worldObjectsToClear) {
            // Only destroy destructible objects.
            if (object.isDestructible()) {
                object.destroy();
                worldObjects.remove(object);
            }
        }
        worldObjectsToClear.clear(); // Remove all references to avoid confusion later.
    }

    /**
     * Get the Box2D instance.
     * @return The Box2D instance.
     */
    public Box2DProcessing getBox2D() {
        return box2D;
    }

    /**
     * Get the physics scale factor.
     * @return The physics scale factor.
     */
    public float getPhysicsScaleFactor() {
        return physicsScaleFactor;
    }
}
