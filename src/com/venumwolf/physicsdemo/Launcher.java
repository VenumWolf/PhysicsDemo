package com.venumwolf.physicsdemo;

import processing.core.PApplet;

public class Launcher {
    public static void main(String[] passedArgs) {
        String[] appletArgs = new String[]{"--present", "--window-color=#666666", "--stop-color=#cccccc",
                "com.venumwolf.physicsdemo.PhysicsDemo"};
        if (passedArgs != null) {
            PApplet.main(PApplet.concat(appletArgs, passedArgs));
        } else {
            PApplet.main(appletArgs);
        }
    }
}
