package com.venumwolf.physicsdemo.physicsobjects.dynamic.simple;


import com.venumwolf.physicsdemo.physicsobjects.AbstractPhysicsObject;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

public class Ball extends AbstractPhysicsObject {
    private float density;
    private float friction;
    private float restitution;
    private float angularVel;
    private float diameter;

    /**
     * Default ball constructor.
     * @param x        The starting y position, in pixels, of the ball.
     * @param y        The starting y position, in pixels, of the ball.
     * @param diameter The diameter, in meters, of the ball.
     */
    public Ball(float x, float y, float diameter) {
        this.bodyType = BodyType.DYNAMIC;

        this.diameter = diameter;
        this.density = 0.1f;
        this.friction = 0.4f;
        this.restitution = 0.6f;

        this.fillColor = physicsDemo.color(0, 0, 0, 0);
        this.strokeColor = physicsDemo.color(255);
        this.strokeWeight = 1;

        float velX = physicsDemo.random(-0, 0);
        float velY = physicsDemo.random(-0, 0);
        this.angularVel = physicsDemo.random(-0, 0);
        makeBody(x, y, velX, velY, angularVel, diameter);
    }

    /**
     * Create a ball with custom display parameters.
     * @param x            The starting y position, in pixels, of the ball.
     * @param y            The starting y position, in pixels, of the ball.
     * @param diameter     The diameter, in meters, of the ball.
     * @param fillColor    The color of the drawn shape.
     * @param strokeColor  The color of the drawn lines.
     * @param strokeWeight The thickness in pixels the outlines should be drawn.
     */
    public Ball(float x, float y, float diameter, int fillColor, int strokeColor, int strokeWeight) {
        super();

        this.bodyType = BodyType.DYNAMIC;

        this.diameter = diameter;
        this.density = 0.1f;
        this.friction = 0.4f;
        this.restitution = 0.6f;

        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWeight = strokeWeight;

        float velX = 0;
        float velY = 0;
        this.angularVel = 0;
        makeBody(x, y, velX, velY, angularVel, diameter);
    }

    /**
     * Create a ball with custom physics properties.
     * @param x           The starting x position, in pixels, of the ball.
     * @param y           The starting y position, in pixels, of the ball.
     * @param diameter    The diameter, in meters, of the ball.
     * @param density     The density, in kg/m^2, of the ball.
     * @param friction    The friction of the ball.
     * @param restitution The restitution, or bounce, of the ball.
     */
    public Ball(float x, float y, float diameter, float density, float friction, float restitution) {
        super();

        this.bodyType = BodyType.DYNAMIC;

        diameter = diameter * physicsDemo.getPhysicsScaleFactor();
        this.density = density;
        this.friction = friction;
        this.restitution = restitution;

        this.fillColor = physicsDemo.color(0, 0, 0, 0);
        this.strokeColor = physicsDemo.color(255);
        this.strokeWeight = 1;

        float velX = 0;
        float velY = 0;
        this.angularVel = 0;
        makeBody(x, y, velX, velY, angularVel, diameter);
    }

    /**
     * Create a ball with custom physics properties and velocity.
     * @param x           The starting x position, in pixels, of the ball.
     * @param y           The starting y position, in pixels, of the ball.
     * @param diameter    The diameter, in meters, of the ball.
     * @param density     The density, in kg/m^2, of the ball.
     * @param friction    The friction of the ball.
     * @param restitution The restitution, or bounce, of the ball.
     * @param velX        The starting x velocity of the ball.
     * @param velY        The starting y velocity of the ball.
     * @param angularVel  The starting angular velocity of the ball.
     */
    public Ball(float x, float y, float diameter, float density, float friction, float restitution, float velX,
                float velY, float angularVel) {
        super();

        this.bodyType = BodyType.DYNAMIC;

        diameter = diameter * physicsDemo.getPhysicsScaleFactor();
        this.density = density;
        this.friction = friction;
        this.restitution = restitution;

        this.fillColor = physicsDemo.color(0, 0, 0, 0);
        this.strokeColor = physicsDemo.color(255);
        this.strokeWeight = 1;

        makeBody(x, y, velX, velY, angularVel, diameter);
    }

    /**
     * Create a ball with fully custom values.
     * @param x            The starting y position, in pixels, of the ball.
     * @param y            The starting y position, in pixels, of the ball.
     * @param diameter     The diameter, in meters, of the ball.
     * @param fillColor    The color of the drawn shape.
     * @param strokeColor  The color of the drawn lines.
     * @param strokeWeight The thickness in pixels the outlines should be drawn.
     * @param diameter     The diameter, in meters, of the ball.
     * @param density      The density, in kg/m^2, of the ball.
     * @param friction     The friction of the ball.
     * @param restitution  The restitution, or bounce, of the ball.
     * @param velX         The starting x velocity of the ball.
     * @param velY         The starting y velocity of the ball.
     * @param angularVel   The starting angular velocity of the ball.
     */
    public Ball(float x, float y, float diameter, int fillColor, int strokeColor, int strokeWeight, float density,
                float friction, float restitution, float velX, float velY, float angularVel) {
        super();

        this.bodyType = BodyType.DYNAMIC;

        this.diameter = diameter;
        this.density = density;
        this.friction = friction;
        this.restitution = restitution;

        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWeight = strokeWeight;

        this.angularVel = angularVel;
        makeBody(x, y, velX, velY, angularVel, diameter);
    }

    /**
     * Creates body and fixture for the ball.
     * @param x          The starting x position, in pixels, of the ball.
     * @param y          The starting y position, in pixels, of the ball.
     * @param velX       The starting x velocity, in m/s, of the ball.
     * @param velY       The starting y velocity, in m/s, of the ball.
     * @param angularVel The starting angular velocity, in m/s, of the ball.
     * @param diameter   The diameter, in meters, of the ball.
     */
    private void makeBody(float x, float y, float velX, float velY, float angularVel, float diameter) {
        CircleShape shape = new CircleShape();
        shape.m_radius = diameter / 2;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position = physicsDemo.getBox2D().coordPixelsToWorld(x, y);
        bodyDef.type = this.bodyType;
        this.body = physicsDemo.getBox2D().world.createBody(bodyDef);

        FixtureDef fixture = new FixtureDef();
        fixture.shape = shape;
        fixture.density = this.density;
        fixture.friction = this.friction;
        fixture.restitution = this.restitution;
        this.body.createFixture(fixture);

        this.body.setLinearVelocity(new Vec2(velX, velY));
        this.body.setAngularVelocity(angularVel);
    }

    /**
     Draws the ball on the screen.
     */
    public final void display() {
        Vec2 pos = physicsDemo.getBox2D().getBodyPixelCoord(this.body);
        float a = this.body.getAngle();
        float displayDiameter = physicsDemo.getBox2D().scalarWorldToPixels(diameter);

        physicsDemo.pushMatrix();
        physicsDemo.translate(pos.x, pos.y);
        physicsDemo.rotate(-a);
        physicsDemo.strokeWeight(this.strokeWeight);

        if (body.isBullet()) {
            physicsDemo.stroke(255, 0, 0);
            physicsDemo.fill(255, 0, 0, 100);
        }
        else if (body.isAwake() && !body.isBullet()) {
            physicsDemo.stroke(this.strokeColor);
            physicsDemo.fill(this.fillColor);
        } else {
            physicsDemo.stroke(physicsDemo.color(200));
            physicsDemo.fill(physicsDemo.color(100, 100, 100, 100));
        }

        physicsDemo.ellipse(0, 0, displayDiameter, displayDiameter);
        physicsDemo.line(0, 0, displayDiameter / 2, 0);
        physicsDemo.popMatrix();
    }
}