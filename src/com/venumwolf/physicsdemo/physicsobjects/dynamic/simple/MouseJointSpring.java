package com.venumwolf.physicsdemo.physicsobjects.dynamic.simple;

import com.venumwolf.physicsdemo.physicsobjects.AbstractPhysicsObject;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.joints.MouseJoint;
import org.jbox2d.dynamics.joints.MouseJointDef;

public class MouseJointSpring extends AbstractPhysicsObject {

    protected MouseJoint mouseJoint;

    public MouseJointSpring() {
        mouseJoint = null;
        this.strokeColor = physicsDemo.color(255);
        this.strokeWeight = 1;
    }

    public final void update() {
        if (this.mouseJoint != null) {
            Vec2 mouseLocation = physicsDemo.getBox2D().coordPixelsToWorld(physicsDemo.mouseX, physicsDemo.mouseY);
            this.mouseJoint.setTarget(mouseLocation);
        }
    }

    public final void display() {
        if (this.mouseJoint != null) {
            Vec2 anchorA = new Vec2(0, 0);
            this.mouseJoint.getAnchorA(anchorA);
            Vec2 anchorB = new Vec2(0, 0);
            mouseJoint.getAnchorB(anchorB);

            anchorA = physicsDemo.getBox2D().coordWorldToPixels(anchorA);
            anchorB = physicsDemo.getBox2D().coordWorldToPixels(anchorB);

            physicsDemo.strokeWeight(this.strokeWeight);
            physicsDemo.stroke(this.strokeColor);
            physicsDemo.line(anchorA.x, anchorA.y, anchorB.x, anchorB.y);
        }
    }

    public final void bind(float x, float y, AbstractPhysicsObject object) {
        MouseJointDef mouseJointDef = new MouseJointDef();

        mouseJointDef.bodyA = physicsDemo.getBox2D().getGroundBody();
        mouseJointDef.bodyB = object.getBody();

        Vec2 mouseLocation = physicsDemo.getBox2D().coordPixelsToWorld(x, y);

        mouseJointDef.target.set(mouseLocation);

        mouseJointDef.maxForce = 100.0f * object.getBody().m_mass;
        mouseJointDef.frequencyHz = 5.0f;
        mouseJointDef.dampingRatio = 0.9f;

        mouseJoint = (MouseJoint) physicsDemo.getBox2D().world.createJoint(mouseJointDef);
    }

    @Override
    public final void destroy() {
        if (mouseJoint != null) {
            physicsDemo.getBox2D().world.destroyJoint(this.mouseJoint);
            this.mouseJoint = null;
        }
    }
}
