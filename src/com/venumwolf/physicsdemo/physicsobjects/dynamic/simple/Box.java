package com.venumwolf.physicsdemo.physicsobjects.dynamic.simple;

import com.venumwolf.physicsdemo.physicsobjects.AbstractPhysicsObject;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

public class Box extends AbstractPhysicsObject {
    private float x;
    private float y;
    private float w;
    private float h;
    private float velX;
    private float velY;
    private float angularVel;
    private float friction;
    private float density;
    private float restitution;

    /**
     * Default box constructor.
     * @param x The starting x position, in pixels, of the box.
     * @param y The starting x position, in pixels, of the box.
     * @param w The width, in meters, of the box.
     * @param h The height, in meters of the box.
     */
    public Box(float x, float y, float w, float h ) {
        this.bodyType = BodyType.DYNAMIC;
        this.x = x;
        this.y = y;
        this.w = w * physicsDemo.getPhysicsScaleFactor();
        this.h = h * physicsDemo.getPhysicsScaleFactor();
        this.fillColor = physicsDemo.color(255, 255, 255, 100);
        this.strokeColor = physicsDemo.color(255);
        this.strokeWeight = 1;
        this.velX = 0;
        this.velY = 0;
        this.angularVel = 0;
        this.density = 2400.0f;
        this.friction = 0.6f;
        this.restitution = 0.3f;
        makeBody();
    }

    /**
     * Create a ball with custom display properties.
     * @param x            The starting x position, in pixels, of the box.
     * @param y            The starting x position, in pixels, of the box.
     * @param w            The width, in meters, of the box.
     * @param h            The height, in meters of the box.
     * @param fillColor    The color of the drawn shape.
     * @param strokeColor  The color of the drawn lines.
     * @param strokeWeight The thickness, in pixels, of the drawn lines.
     */
    public Box(float x, float y, float w, float h, int fillColor, int strokeColor, int strokeWeight) {
        this.bodyType = BodyType.DYNAMIC;
        this.x = x;
        this.y = y;
        this.w = w * physicsDemo.getPhysicsScaleFactor();
        this.h = h * physicsDemo.getPhysicsScaleFactor();

        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWeight = strokeWeight;

        this.density = 2400.0f;
        this.friction = 0.6f;
        this.restitution = 0.3f;

        this.velX = 0;
        this.velY = 0;
        this.angularVel = 0;

        makeBody();
    }

    /**
     * Create a box with custom physics properties.
     * @param x            The starting x position, in pixels, of the box.
     * @param y            The starting x position, in pixels, of the box.
     * @param w            The width, in meters, of the box.
     * @param h            The height, in meters of the box.
     * @param density
     * @param friction
     * @param restitution
     */
    public Box(float x, float y, float w, float h, float density, float friction, float restitution, float velX, float velY, float angularVel) {
        this.bodyType = BodyType.DYNAMIC;
        this.x = x;
        this.y = y;
        this.w = w * physicsDemo.getPhysicsScaleFactor();
        this.h = h * physicsDemo.getPhysicsScaleFactor();
        this.fillColor = physicsDemo.color(255, 255, 255, 100);
        this.strokeColor = physicsDemo.color(255);
        this.strokeWeight = 1;
        this.velX = velX;
        this.velY = velY;
        this.angularVel = angularVel;
        this.density = density;
        this.friction = friction;
        this.restitution = restitution;
        makeBody();
    }

    private void makeBody() {
        // Define shape.
        PolygonShape shape = new PolygonShape();
        float box2dW = physicsDemo.getBox2D().scalarPixelsToWorld(this.w / 2);
        float box2dH = physicsDemo.getBox2D().scalarPixelsToWorld(this.h / 2);
        shape.setAsBox(box2dW, box2dH);

        // Define and create body.
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = this.bodyType;
        bodyDef.position.set(physicsDemo.getBox2D().coordPixelsToWorld(new Vec2(this.x, this.y)));
        this.body = physicsDemo.getBox2D().world.createBody(bodyDef);

        // Define fixture.
        FixtureDef fixture = new FixtureDef();
        fixture.shape = shape;
        fixture.density = this.density;
        fixture.friction = this.friction;
        fixture.restitution = this.restitution;
        this.body.createFixture(fixture);

        // Set object velocity.
        this.body.setLinearVelocity(new Vec2(this.velX, this.velY));
        this.body.setAngularVelocity(this.angularVel);
    }

    public final void display() {
        Vec2 pos = physicsDemo.getBox2D().getBodyPixelCoord(body);
        float a = this.body.getAngle();
        physicsDemo.pushMatrix();
        physicsDemo.translate(pos.x, pos.y);
        physicsDemo.rotate(-a);
        physicsDemo.strokeWeight(1);
        if (body.isBullet()) {
            physicsDemo.stroke(255, 0, 0);
            physicsDemo.fill(255, 0, 0, 100);
        }
        else if (body.isAwake() && !body.isBullet()) {
            physicsDemo.stroke(this.strokeColor);
            physicsDemo.fill(this.fillColor);
        } else {
            physicsDemo.stroke(physicsDemo.color(200));
            physicsDemo.fill(physicsDemo.color(100, 100, 100, 100));
        }
        physicsDemo.rectMode(physicsDemo.CENTER);
        physicsDemo.rect(0, 0, w, h);
        physicsDemo.popMatrix();
    }
}
