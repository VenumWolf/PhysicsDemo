package com.venumwolf.physicsdemo.physicsobjects.fixed.simple;

import com.venumwolf.physicsdemo.physicsobjects.AbstractPhysicsObject;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;

public class Wall extends AbstractPhysicsObject {
    private float x;
    private float y;
    private float w;
    private float h;

    public Wall(float x, float y, float w, float h, boolean isDestructible) {
        this.bodyType = BodyType.STATIC;
        this.x = x;
        this.y = y;
        this.w = w * physicsDemo.getPhysicsScaleFactor();
        this.h = h * physicsDemo.getPhysicsScaleFactor();
        this.isDestructible = isDestructible;
        this.strokeColor = physicsDemo.color(255);
        this.fillColor = physicsDemo.color(0, 0, 0, 0);
        this.strokeWeight = 1;
        makeBody();
    }

    private void makeBody() {
        PolygonShape shape = new PolygonShape();
        float box2dW = physicsDemo.getBox2D().scalarPixelsToWorld(this.w / 2);
        float box2dH = physicsDemo.getBox2D().scalarPixelsToWorld(this.h / 2);
        shape.setAsBox(box2dW, box2dH);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = this.bodyType;
        bodyDef.position.set(physicsDemo.getBox2D().coordPixelsToWorld(this.x, this.y));
        this.body = physicsDemo.getBox2D().createBody(bodyDef);

        this.body.createFixture(shape, 1);
    }

    /**
     The body is static, updating does absolutely nothing.
     */
    public final void update() {
    }

    /**
     Draw the wall on screen.
     */
    public final void display() {
        physicsDemo.fill(this.fillColor);
        physicsDemo.stroke(this.strokeColor);
        physicsDemo.strokeWeight(1);
        physicsDemo.rectMode(physicsDemo.CENTER);
        physicsDemo.rect(x, y, w, h);
    }
}
