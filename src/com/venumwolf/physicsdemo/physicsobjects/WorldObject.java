package com.venumwolf.physicsdemo.physicsobjects;

interface WorldObject {
    boolean isDestructible = true;

    /**
     Update dynamic values of the object.
     */
    void update();
    /**
     Display the object on the object on the screen.
     */
    void display();

    /**
     Check if a point falls within the object.
     @param x The x position, in pixels, to check.
     @param y The y position, in pixels, to check.
     */
    boolean contains(int x, int y);

    /**
     Destroy the object's physics body.
     */
    void destroy();

    /**
     * Check if the object is destructible.
     * @return True object should be destructible.
     */
    boolean isDestructible();
}
