package com.venumwolf.physicsdemo.physicsobjects;

import com.venumwolf.physicsdemo.PhysicsDemo;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.Fixture;

/**
 * Contains the basic functionality / definition for any object that will be interacting with the Physics Demo.
 * <br>
 * The main object loop is polymorphic and expects objects of type {@link AbstractPhysicsObject},  so any objects
 * that will be interacting with the physics demo must extend this class.
 */
public abstract class AbstractPhysicsObject implements WorldObject {
    protected Body body;
    protected BodyType bodyType;
    protected int fillColor;
    protected int strokeColor;
    protected int strokeWeight;
    protected boolean isDestructible;

    // Objects need a reference to the active PhysicsDemo for rendering, and interaction with the world.
    protected PhysicsDemo physicsDemo;

    /**
     * Default Constructor.
     */
    public AbstractPhysicsObject() {
        physicsDemo = PhysicsDemo.getPhysicsDemo();
        isDestructible = true;
    }

    public abstract void display();

    /**
     * Update any dynamic attributes of the object.
     * <br>
     * The default update() automatically marks the physics body as a bullet if it is traveling fast enough.
     * It is evident that this will negatively impact performance when many objects are moving at high speeds;
     * however, it will make small, fast moving objects less likely to phase though other objects in the world.
     * For performance reasons, this default bullet behavior should be overridden and removed for larger objects
     * (anything larger than 1 m x 1 m), or if many instances of an object are expected to be moving quickly.
     */
    public void update() {
        float bulletThreshold = 20.0f;
        if (
                (
                        body.getLinearVelocity().x > bulletThreshold ||
                                body.getLinearVelocity().x < (bulletThreshold * -1)
                ) || (
                        body.getLinearVelocity().y > bulletThreshold ||
                                body.getLinearVelocity().y < (bulletThreshold * -1))
        ) {
            body.setBullet(true);
        } else {
            body.setBullet(false);
        }
    }

    /**
     * Default contains always returns false.
     * Use to check if a point is within an object.
     */
    public boolean contains(int x, int y) {
        if (body != null) {
            Fixture fixture = body.getFixtureList();
            return fixture.testPoint(new Vec2(physicsDemo.getBox2D().coordPixelsToWorld(new Vec2(x, y))));
        }
        return false;
    }

    /**
     * Default isDestructible always returns true.
     * @return True if the object can be removed.
     */
    public boolean isDestructible() {
        return isDestructible;
    }

    /**
     * Destroys the Box2D body associated with the object. Any external references will have to be removed externally for
     * GC to do it's job.
     */
    public void destroy() {
        if (body != null && isDestructible()) {
            physicsDemo.getBox2D().world.destroyBody(body);
            body = null;
        }
    }

    public Body getBody() {
        return body;
    }
}

